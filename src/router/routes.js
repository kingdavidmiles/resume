const routes = [
  {
    path: "/",
    component: () => import("layouts/MyLayout.vue"),
    children: [{ path: "/", component: () => import("pages/Homepage.vue") }]
  },

  {
    path: "/",
    component: () => import("layouts/MyLayout.vue"),
    children: [
      // { path: "", component: () => import("pages/Index.vue") },
      // { path: "/home", component: () => import("pages/Homepage.vue") },
      { path: "/myresume", component: () => import("pages/resume.vue") },
      { path: "/about", component: () => import("pages/about.vue") },
      { path: "/profile", component: () => import("pages/profile.vue") },
      { path: "/project", component: () => import("pages/project.vue") },
      { path: "/contact", component: () => import("pages/contact.vue") },
      { path: "/team", component: () => import("pages/team.vue") }
    ]
  }
];

// Always leave this as last one
if (process.env.MODE !== "ssr") {
  routes.push({
    path: "*",
    component: () => import("pages/Error404.vue")
  });
}

export default routes;
